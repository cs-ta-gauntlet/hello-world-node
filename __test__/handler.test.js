import {hello_world, sum} from '../handler';

test("test return hello world", () => {
    expect(hello_world()).toEqual("Hello World from NodeJS 8!");
});

test("test return correct sum", () => {
   expect(sum({'data':{'values':[2,3]}})).toEqual(5);
});