'use strict';

const _ = require('lodash');

module.exports = {
  hello_world(event, context) {
    return "Hello World from NodeJS 8!";
  },
  sum(event, context){
    let values = event.data.values;
    let result = 0;
    for(let i=0; i<values.length; i++){
      result += values[i]
    }
    return result;
  }
};
